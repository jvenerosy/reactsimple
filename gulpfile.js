
// dépendances
var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var copy = require('copy');

//////////////////
// -SCSS/CSS/FONTS
/////////////////

var SCSS_SRC = './src/style/sass/**/*.scss';
var SCSS_DEST = './src/style/css';
var FONTS_SRC = './src/fonts/**/*';
var FONTS_DEST = './src/style/css/fonts';

// Copie du répertoire fonts vers le bon répertoire de destination
gulp.task('copyfonts', function (cb) {
    copy(FONTS_SRC, FONTS_DEST, cb);
});

// Compiler le SCSS
gulp.task('compile_scss', function() {

    gulp.src(SCSS_SRC)
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError)) //on compile le SCSS
    .pipe(autoprefixer({
        browsers: ['last 4 versions']
    }))
    .pipe(minifyCSS()) //minification du CSS
    //.pipe(rename({ suffix: '.min' })) //on rajoute le suffixe '.min'
    .pipe(gulp.dest(SCSS_DEST)) //génère le fichier dans le bon répertoire de destination
    .pipe(browserSync.stream())
});

// détecte les changements dans le SCSS
gulp.task('watch_scss', function() {
    gulp.watch(SCSS_SRC, [
        'compile_scss',
        'copyfonts'
    ]);
});


// Exécute les tâches
gulp.task('default', [
    'compile_scss',
    'copyfonts',
    'watch_scss'
]);
