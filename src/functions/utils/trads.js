import fr from "../../data/trad/fr";
import be from "../../data/trad/be";
import es from "../../data/trad/es";

import { getLanguage } from "./getLanguage";

const languages = {
    "fr": fr,
    "be": be,
    "es": es
};

export const lang = getLanguage();
export default languages[lang];