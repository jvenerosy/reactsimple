
export function getLanguage() {
    let lang = new URLSearchParams(window.location.search).get('lang');
    if (lang === null || lang === undefined) {
        lang = localStorage.hasOwnProperty("lang") ? localStorage.lang : null;
    } else {
        localStorage.setItem("lang", lang);
    }
    if (lang === null || lang === undefined) { //si il n'y a rien dans l'URL ou dans le localStorage
        lang = "fr";
        localStorage.setItem("lang", lang);
    }

    return lang;
}