import $ from "jquery";
import debug from './debug';

export default function (jQuerySelector, imgLoaded) {

    jQuerySelector = ':not(video)' + jQuerySelector;

    $.fn.loadImages = function () {

        let loadedCount = 0;
        let self = this;
        let imgSrc = [];

        // Collect each element's image src
        self.each(function () {

            let elem = $(this);

            // Check if the element is an image tag
            if (elem.attr('src')) {
                // If so, just add its src to the list of images to load
                imgSrc.push(elem.attr('src'));
            }

            // Check if the element has a background image
            if (elem.css('background-image') !== 'none') {
                let bgImgs = elem.css('background-image').split(', ');

                // The element can have multiple background images so we have to loop and add each of them to the list of images to load
                $.each(bgImgs, function (i, urlSrc) {
                    // Clean the src by removing "url('')" around it
                    let src = urlSrc.replace(/^url\(["']?/, '').replace(/["']?\)$/, '');
                    imgSrc.push(src);
                });
            }
        });

        if (imgSrc.length > 0) {
            $.each(imgSrc, function (i, src) {

                // Force browser to load every image by putting them in an invisible img tag
                // And track when each image is loaded via the load event
                $('<img/>').attr('src', src).on('load', function () {
                    $(this).remove();
                    loadedCount++;

                    debug('Image loaded : ' + src + ' (' + loadedCount + '/' + imgSrc.length + ')');

                    // Once every image is loaded, end the image loader
                    if (loadedCount >= imgSrc.length) {
                        imgLoaded(loadedCount);
                    }
                });

            });
        }
        else {
            imgLoaded(loadedCount);
        }

    };

    $(jQuerySelector).loadImages();
}
