import $ from "jquery";

export default function (jQuerySelector, videosLoaded) {

    jQuerySelector = 'video' + jQuerySelector;

    let totalPercentage = 0;

    function updateProgress(evt, id, controlLoader = false) {
        if (evt.lengthComputable) {
            totalPercentage = (evt.loaded / evt.total) * 100;

            if (totalPercentage === 100) setTimeout(function () {
                if (controlLoader) {
                    videosLoaded();
                }
            }, 100);
        }
    }

    function loadMedia(url, mediaObject, controlLoader = false, nextToLoad = null) {
        // console.log('Loading : ', url);
        let req = new XMLHttpRequest();
        req.open('GET', url, true);
        req.setRequestHeader("Cache-Control", "public, max-age=31536000");
        req.responseType = 'blob';

        req.addEventListener("progress", function (evt) {
            updateProgress(evt, mediaObject.id, controlLoader);
        }, false);

        req.onload = function () {
            // Onload is triggered even on 404
            // so we need to check the status code
            // and if the resource is ready
            if (this.readyState === 4 && this.status === 200) {
                let blobData = this.response;
                let objectURL = URL.createObjectURL(blobData); // IE10+
                // Media is now downloaded
                // and we can set it as source on the media element
                // mediaObject.src = objectURL;
                $(mediaObject).append('<source src="' + objectURL + '">');
                //let source = $('#' + mediaObject.id + ' source');
                //source.attr('src', objectURL);
                mediaObject.load();
                //console.log('Completely loaded: ' + mediaObject.id);
                setTimeout(function () {
                    if (nextToLoad !== null) {
                        loadMedia(nextToLoad.url, nextToLoad.mediaObject, nextToLoad.controlLoader, nextToLoad.nextToLoad);
                    }
                }, 500);
            }
        };
        req.onerror = function () {
            // An Error Occurred
            //document.location.reload();
            console.log('Erreur');
        };

        req.send();
    }

    let videoToLoad = null, tmp = null;
    let listVideos = [];

    if ($(jQuerySelector).length > 0) {

        // Collect all videos sources and their respective DOM element
        $(jQuerySelector).each(function () {
            let element = $(this);
            if (element.data('source')) {
                let sources = element.data('source').split(',');
                $.each(sources, function (key, src) {
                    listVideos.push({
                        url: src.trim(),
                        mediaObject: element[0]
                    });
                });
            }
        });

        // Chain and store videos in a single object
        for (let i = 0; i < listVideos.length; i++) {
            if (i === 0) {
                // First video
                tmp = {
                    url: listVideos[i].url,
                    mediaObject: listVideos[i].mediaObject,
                    controlLoader: (i === (listVideos.length - 1)),
                    nextToLoad: null
                };
                videoToLoad = tmp;
            } else {
                // The next videos are stored in the nextToLoad property of the previous video
                tmp.nextToLoad = {
                    url: listVideos[i].url,
                    mediaObject: listVideos[i].mediaObject,
                    controlLoader: (i === listVideos.length - 1),
                    nextToLoad: null
                };
                tmp = tmp.nextToLoad;
            }
        }

        // Load videos
        loadMedia(videoToLoad.url, videoToLoad.mediaObject, videoToLoad.controlLoader, videoToLoad.nextToLoad);
    }
    else {
        videosLoaded();
    }

}
