import config from "../../data/config/fr";

export default function(text, logType = 'log') {
    if(config.environment === 'development') {
        switch (logType) {
            case 'warn':
                console.warn(text);
                break;

            case 'info':
                console.info(text);
                break;

            case 'error':
                console.error(text);
                break;

            case 'log':
            default:
                console.log(text);
                break;
        }
    }
};