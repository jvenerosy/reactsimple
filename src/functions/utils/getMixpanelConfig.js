import config from '../../data/config/fr';

export function getMixpanelConfig() {
    var urlo = window.location.href,
        urlo1 = urlo.substr(0, 9),
        urlo2 = urlo.substr(0, 16),
        localhost = "http://localhost",
        df = "http://df",
        ip = "http://10";

        var mixpanelConfig = {};

        if (urlo1 === df ||  urlo2 === localhost || urlo1 === ip) {
            mixpanelConfig = {
                token: config.mixpanel.preprod.token,
                apiHost: config.mixpanel.preprod.apiHost
            }
        } else {
            mixpanelConfig = {
                token: config.mixpanel.preprod.token,
                apiHost: config.mixpanel.preprod.apiHost
            }
        }

    return mixpanelConfig;
}