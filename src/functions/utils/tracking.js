import config from '../../data/config/fr';

import $ from 'jquery';
import mixpanel from 'mixpanel-browser';

import { getCookieID } from './getCookieID';

export function tracking() {
    let data = config;

    /********************* RECUPERER LES PROPRIETES ****************************/

    // Récupération du info member
    var infoMember = getCookieID('infoMember');
    infoMember = ( infoMember != null ) ? infoMember.substr(4) : data.operation.code + '_member'; // VALEUR FINALE

    $('.event').on('click', function () {

        let event = $(this).attr("data-event"),
            label = $(this).attr("data-label"),
            prefix = "";

        switch (event) {
            case 'TagEvent::Marketing':
                break;
            case 'TagEvent::Exit':
                prefix = "EXIT_";
                break;
            case 'TagEvent::Page':
                break;
            default:
                break;
        }

        // Mixpanel
        mixpanel.track("Click",{
            "Operation Code": data.operation.code,
            "Click Name":"Mini site " + prefix + label
        });

        // Adotmob
        if (data.modules.adotmob) {
            window.adotmob.triggerEvent(infoMember, data.operation.code, "Mini site " + prefix + label);
        }
    });
}