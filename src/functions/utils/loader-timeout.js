import $ from "jquery";
import config from "../../data/config/fr";
import debug from './debug';

export default function () {

    debug('Error : True loader is not set. Please add `' + config.loader.jQuerySelector + '` to specify images that must be loaded before closing the loader.', 'error');

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function checkCookie() {
        let loaderactif = getCookie("loaderactif");
        let timer = 0;
        if (loaderactif !== "true") {
            document.cookie = "loaderactif=true";
            timer = 4500;
        }
        setTimeout(function () {
            $(document).closeLoader();
        }, timer);
    }

    checkCookie();

}
