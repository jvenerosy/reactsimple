export function getCookieID(sName) {
    var cookContent = document.cookie, cookEnd, i, j;
    sName = sName + "=";
    var c;

    for (i = 0, c = cookContent.length; i < c; i++) {
        j = i + sName.length;
        if (cookContent.substring(i, j) === sName) {
            cookEnd = cookContent.indexOf(";", j);
            if (cookEnd === -1) {
                cookEnd = cookContent.length;
            }
            return decodeURIComponent(cookContent.substring(j, cookEnd));
        }
    }
    return null;
}