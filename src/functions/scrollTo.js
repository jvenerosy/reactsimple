import $ from "jquery";

export function scrollTo(trigger) {

    $(trigger).each(function () {

        $(this).click(function () {
            var destination = $(this).attr("data-scroll");

            if (isNaN(destination)) {
                //si la destination n'est pas un nombre on scroll jusqu'a un element en particulier
                destination = $(destination).offset().top;

                console.log('headerH', $('.header').height())
            }

            var bpMobile = 1025;
            var plusHeader = ($(window).width() < bpMobile) ? + 50 : $('.header').height() + 90;

            $('html, body').animate({ scrollTop: destination - plusHeader }, 800);

            return false;
        });
    });
}