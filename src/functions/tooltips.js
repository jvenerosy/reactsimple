import $ from 'jquery';

export function tooltips() {

    var hash = window.location.hash;

    if (hash === '#tracking') {

        $('.event').each(function () {
            $(this).addClass('tooltip');
            var dataLabel = $(this).data('label'),
                dataEvent = $(this).data('event');

            $(this).attr('title', dataLabel);
            $(this).find('img').each(function () {
                var imgW = $(this).width();
                var imgH = $(this).height();

                $(this).closest('.tooltip').addClass('tooltip--img');
                $('.tooltip--img img').after('<p className="tooltip__content tooltip__content--img"><span className="imgwording">IMAGE : </span>' + dataLabel + '</p>');
                $('.tooltip--img .tooltip__content').width(imgW);
                $('.tooltip--img .tooltip__content').height(imgH);
                $(this).find('img').hide();
            })

            if (dataEvent === "TagEvent::Exit") {
                dataLabel = "Exit_" + dataLabel;
            }

            $(this).html('<span className="tooltip__content">' + dataLabel + '</span>')
        });
    }

}