import timeoutLoader from './utils/loader-timeout';
import imageLoader from './utils/loader-images';
import videoLoader from './utils/loader-videos';
import debug from './utils/debug';
import $ from "jquery";
import config from "../data/config/fr";

export function loader() {

    let finishedLoading = {images: false, videos: false};
    let jQuerySelector = config.loader ? config.loader.jQuerySelector : null;

    $.fn.closeLoader = function() {
        $(".loader").fadeOut("100");
    };

    let videosLoaded = function() {
        finishedLoading.videos = true;
        isDoneLoading();
    };

    let imgLoaded = function(loadedCount) {
        debug('Successfully loaded ' + loadedCount + ' images');
        finishedLoading.images = true;
        isDoneLoading();
    };

    function isDoneLoading () {
        let done = true;
        $.each(finishedLoading, function(key, val) {
            done = done && val;
        });

        if(done) $(document).closeLoader();
    }

    let hasSelector = ( (config.loader !== undefined)
        && (jQuerySelector !== undefined)
        && (jQuerySelector !== null));

    if(hasSelector && $(jQuerySelector).length > 0) {
        imageLoader(jQuerySelector, imgLoaded);
        videoLoader(jQuerySelector, videosLoaded);
    } else timeoutLoader();

}
