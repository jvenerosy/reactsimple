import React, { Component } from 'react';

import $ from "jquery";

import trad from "../../../data/trad/fr";

class HeaderMenu extends Component {

    openORclose(event) {
        $('.header__menu').slideToggle();
        $(event.target).toggleClass('active');
        $('.js-waypoint-content').toggleClass('in-header');
    }

    render() {
        return (
            <div>
                <div className="header__padding"></div>
                <header className="header__menu">
                    <a href="https://secure.fr.vente-privee.com/ns/fr-fr/home/default" className="header__menu__back-to-vp event" data-event="TagEvent::Exit" data-label="Back to VP">
                        <img src={require("../../../img/back-to-vp.png")} alt="Retourner sur vente-privee" />
                    </a>
                    <div className="header__menu__navbar no-desktop">
                        <div className="header__menu__navbar__hamburger js-menu" onClick={this.openORclose}></div>
                        <img className="header__menu__navbar__logo" src={require("../../../img/logo-kerastase-black.png")} alt="Kérastase Paris" />
                    </div>
                    <div className="header__menu__menu">
                        <div className="header__menu__container">
                            <div className="header__menu__menu__logo desktop-only">
                                <img className="header__menu__navbar__logo" src={require("../../../img/logo-kerastase-black.png")} alt="Kérastase Paris" />
                            </div>
                            <ul>
                                {trad.menus.map((menu, i) => { //on boucle
                                    return (
                                        // eslint-disable-next-line
                                        <li key={i} className={"header__menu__menu__item " + menu.additionalClass}><a href="#" className="header__menu__menu__link js-menu-nav event" data-label={menu.title} data-event="TagEvent::Page">{menu.title}</a></li>
                                    );
                                })}
                                <li className="header__menu__menu__item"><a href={trad.link.generic} className="header__menu__menu__link header__menu__link--exit event" target="_blank" data-label="Trouvez un salon" data-event="TagEvent::Exit">{trad.menu_external}</a></li>
                            </ul>
                        </div>
                    </div>
                </header>
            </div>
        );
    }
}

export default HeaderMenu;