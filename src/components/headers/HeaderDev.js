import React, { Component } from 'react';

import classNames from 'classnames';

class HeaderDev extends Component {

    miniSiteUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;

    constructor(props) {
        super(props);

        this.state = {
            headerDevClosed: true,
            appPopinClosed: true,
        };
    }

    componentDidMount() {

        let hash = window.location.hash;

        // Open devToolbar when hash is #dev
        if (hash === "#dev") {
            this.openHeaderDev();
        }

        // Show a popin to open mini site in VP App
        if (hash === "#app") {
            this.openAppPopin();
        }
    }

    getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=([^&;]+?)(&|#|;|$)').exec(window.location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
    }

    clickMixpanel() {
        let mixpanel = this.getURLParameter('mp_properties');
        let url;

        let hash = window.location.hash;

        if (!mixpanel) {
            let token = 'eyJJbnRlcmZhY2UiOiJTbWFydHBob25lIEFuZHJvaWQgYXBwIiwiVVRNIFRlcm0gW0xhc3QgVG91Y2hdIjoiT3JnYW5pYyIsIlVUTSBDb250ZW50IFtMYXN0IFRvdWNoXSI6Ik9yZ2FuaWMiLCJVVE0gQ2FtcGFpZ24gW0xhc3QgVG91Y2hdIjoiT3JnYW5pYyIsIlVUTSBNZWRpdW0gW0xhc3QgVG91Y2hdIjoiT3JnYW5pYyIsIlVUTSBTb3VyY2UgW0xhc3QgVG91Y2hdIjoiT3JnYW5pYyIsIkNSTSBTZWdtZW50IjoxLCIjIG9mIENvbXBsZXRlZCBQdXJjaGFzZXMiOjI1NiwiTWVtYmVyIElEIjoxNTc3Mzk4NiwiUG9pbnQgcmVhY2hlZCBvbiBob21lcGFnZSI6NCwiT3B0aW1pemVseSBDdXJyZW50IEFcL0IgVGVzdHMiOlsiNzg3Njk0MDAxNCAtIFZhcmlhdGlvbiBSYW5kb20iXSwiU291cmNlIjoiT3JnYW5pYyIsIlBvaW50IFJlYWNoZWQgT24gSG9tZXBhZ2UiOjR9';
            url = window.location.host + '?mp_properties=' + token;
        } else {
            url = window.location.host;
        }
        console.log(url);

        window.location = 'http://' + url + hash;
    }

    clickSwap() {
        let swap = window.location.hash;

        let url;

        if (swap !== '#swap') {
            url = window.location.host + '#swap';
            window.location.href = 'http://' + url;
            window.location.reload();
        } else {
            url = window.location.host;
            window.location.href = 'http://' + url;
        }
    }

    clickTracking() {
        let tracking = window.location.hash;

        let url;

        if (tracking !== '#tracking') {
            url = window.location.host + '#tracking';
            window.location.href = 'http://' + url;
            window.location.reload();
        } else {
            url = window.location.host;
            window.location.href = 'http://' + url;
        }
    }

    closeHeaderDev = () => {
        this.setState({ headerDevClosed: true });
    }

    openHeaderDev = () => {
        this.setState({ headerDevClosed: false });
    }

    openAppPopin = () => {
        this.setState({ appPopinClosed: false });
    }

    closeAppPopin = () => {
        window.location.href = this.miniSiteUrl;
    }

    openApp = () => {
        window.location.href = 'appvp://openwv/' + encodeURIComponent(this.miniSiteUrl);
    }

    render() {
        return (
            <div>
                <header className={classNames("header-dev", { "header-dev--closed": this.state.headerDevClosed })}>
                    <div className="header-dev__content">
                        <h3 className="header-dev__title">devToolbar //</h3>
                        <div className="header-dev__btn">
                            <button className="header-dev__swap" onClick={this.clickSwap}>SWAP</button>
                            <button className="header-dev__mixpanel" onClick={this.clickMixpanel}>MIXPANEL</button>
                            <button className="header-dev__tracking" onClick={this.clickTracking}>TRACKING</button>
                            <button className="header-dev__open-app" onClick={this.openApp}>APP VP</button>
                        </div>
                        <p className="header-dev__close" onClick={this.closeHeaderDev}>Close X</p>
                    </div>
                </header>

                <div className={classNames("app-popin", { "app-popin--closed": this.state.appPopinClosed })}>
                    <div className="app-popin__content">
                        <p className="app-popin__txt">Ouvrir le mini site dans l'app VP ?</p>
                        <div>
                            <span className="btn app-popin__btn app-popin__btn--cancel js-close-app-popin" onClick={this.closeAppPopin}>Annuler</span>
                            <span className="btn app-popin__btn app-popin__btn--ok js-open-app" onClick={this.openApp}>Oui</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HeaderDev;