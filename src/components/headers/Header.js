import React, { Component } from 'react';

class Header extends Component {

    render() {
        return (
            <header className="header" ref="headerRef">
                <a href="https://secure.fr.vente-privee.com/ns/fr-fr/home/default" className="header__back-to-vp event" data-event="TagEvent::Exit" data-label="Back to VP">
                    <img src={require("../../img/back-to-vp.png")} alt="Retourner sur vente-privee" />
                </a>
                <div className="header__hero header__container">
                    <h1 className="header__hero__title">
                        <i className="icon icon-logo-vp"></i>
                    </h1>
                    <h2 className="header__hero__sub">
                        <span>Welcome in</span>
                        <span>BaseSimple3 !</span>
                    </h2>
                    <div className="header__hero__cta">
                        <a className="btn event scrollTo" data-scroll=".screen--2" data-event="TagEvent::Page" data-label="Découvrir">Scroooooll !</a>
                    </div>
                    <div className="header__hero__comment">
                        <span className="txt">Hauteur adapté aux mobiles</span>
                        <span className="arrow animated infinite bounce delay-2s">
                            <i className="icon icon-arrow-basic"></i>
                        </span>
                    </div>
                </div>
            </header>
        );
    }
}

export default Header;