import React, { Component } from 'react';

import SlickSlider from 'react-slick';

import trad from '../../data/trad/fr';

class Slider extends Component {

    render() {
        var l = [0, 1, 2];

        var settings = {
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        }

        return (
            <div className="screen screen--1 container">
                <SlickSlider className="slider slick-dotted" {...settings}>
                    {l.map(function (i) { //on boucle
                        return (
                            trad.blocks.map((slider) => { //on boucle
                                return (
                                    <div className="slider__content">
                                        <div>
                                            <div className="slider__inner">
                                                <img className="load-media" src="http://placehold.it/350x150" alt={slider.product.title} />
                                                <strong className="face__model">{slider.product.title}</strong> <span className="face__title">{slider.title}</span>
                                                <p className="face__actions">{slider.product.id}</p>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        )
                    })}
                </SlickSlider>
            </div>
        );
    }
}

export default Slider;