import React, { Component } from 'react';

import parser from 'html-react-parser'; //Afin de parser le contenu JSON contenant des balises HTML en 'vrai' balises HTML

import axios from 'axios';

import mixpanel from 'mixpanel-browser';

import classNames from 'classnames';

import trad from '../../data/trad/fr';
import config from '../../data/config/fr';

var conf = require("../../conf/appsettings.json");

axios.defaults.withCredentials = true;

class Form extends Component {

    constructor(props) {
        super(props);
        this.state = {
            valueInputRules: false,
            valueInputOffers: false,
            lastName: "",
            firstName: "",
            mail: "",
            formInnerOFF: false,
            formResultON: false,
            inputWrapChecked: false
        };

        this.animationOnCheck = this.animationOnCheck.bind(this);
        this.submit = this.submit.bind(this);
    }

    animationOnFocus(event) {
        var input = event.target; //on récupère l'élément input
        input.closest(".form__inputwrap").classList.add("focus");
    }

    animationOnBlur(event) {
        var input = event.target; //on récupère l'élément input
        if (input.value === "") {
            input.closest(".form__inputwrap").classList.remove("focus");
        }
    }

    animationOnCheck(event) {
        var inputCheckbox = event.target; //on récupère l'élément input checkbox;
        var span = inputCheckbox.closest(".form__inputwrap"); //on récupère son 1er parent qui est un span
        span.classList.toggle("checked");
        span.classList.toggle("full");

        if (span.classList.contains("checked")) {
            if(inputCheckbox.id === "rules") {
                this.setState({ valueInputRules: true });
            }
            else {
                this.setState({ valueInputOffers: true });
            }
        }
        else {
            if(inputCheckbox.id === "rules") {
                this.setState({ valueInputRules: false });
            }
            else {
                this.setState({ valueInputOffers: false });
            }
        }
    }

    animationOnKeyUp(event) {
        var input = event.target; //on récupère l'élément input;

        if (input.value !== "") {
            input.closest(".form__inputwrap").classList.add("full");
            input.closest(".form__inputwrap").classList.remove("error");
        }
        else {
            input.closest(".form__inputwrap").classList.remove("full");
        }
    }

    submit(event) {
        event.preventDefault();

        var lastname = document.getElementById("lastname");
        var firstname = document.getElementById("firstname");
        var mail = document.getElementById("mail");
        var rules = document.getElementById("rules");
        var offers = document.getElementById("offers");

        var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

        var regexMail = true;
        var inputChecked = true;

        if (!testEmail.test(mail.value)) {
            mail.closest(".form__inputwrap").classList.add("full");
            regexMail = false;
        }

        var checkboxes = document.getElementsByClassName("form__inputwrap--check");

        if(!checkboxes[0].classList.contains("checked")){
            rules.closest(".form__inputwrap").classList.add("error");
            inputChecked = false;
        }
       

        var inputTexts = document.getElementsByClassName("form__input--text");

        for(var j = 0; j < inputTexts.length; j++) {
            if(inputTexts[j].value === "") {
                inputTexts[j].closest(".form__inputwrap").classList.add("error");
            }
        }

        var data = {
            "email": mail.value,
            "lastname": lastname.value,
            "firstname": firstname.value,
            "optinRules": rules.value,
            "optinOffers": offers.value
        };

        if(regexMail && inputChecked && lastname.value !== "" && firstname.value !== "") {
            axios.post(conf.urlApi + "projects/exemple/users?mail=other", data, { headers: { 'Content-Type': 'application/json'}}).then(
                (res) => {
                    console.log(res.data);

                    var libell = 'submit-form-ok';
                    mixpanel.track("Click", {
                        "Operation Code": config.operation.code,
                        "Click Name": "Mini site " + libell
                    });

                    this.setState({formInnerOFF: true});
                    this.setState({formResultON: true});

                    setTimeout(() => {
                        var inputWrap = document.getElementsByClassName("form__inputwrap");
                        for(var i = 0; i < inputWrap.length; i++) {
                            inputWrap[i].classList.remove("error");
                            inputWrap[i].classList.remove("full");
                        }
                        for(var j = 0; j < inputTexts.length; j++) {
                            inputTexts[j].value = "";
                        }
                        this.setState({inputWrapChecked: false});
                        this.setState({formInnerOFF: false});
                        this.setState({formResultON: false});
                    }, 3000)

                },
                (err) => {
                    console.log("error=", err);
                }
            );
        }
        
    }

    render() {
        return (
            <div className="screen screen--5 sendmail">
                {/*<p className="sendmail__txt txt">{ trad.sendmail.txt }</p>*/}

                <form className="sendmail__form form" action="#" method="post">
                    <fieldset className="sendmail__fieldset form__fieldset">
                        <div className={classNames("form__inner", {"off": this.state.formInnerOFF})}>
                            <h3 className="sendmail__form-title title">{parser(trad.form.title)}</h3>

                            <div className="form__field">
                                <span className="form__inputwrap form__inputwrap--text">
                                    <label className="form__label txt" htmlFor="lastname">{trad.form.lastname}</label>
                                    <input id="lastname" className="sendmail__input form__input form__input--text" type="text" onFocus={this.animationOnFocus} onBlur={this.animationOnBlur} onKeyUp={this.animationOnKeyUp} />
                                </span>
                            </div>

                            <div className="form__field">
                                <span className="sendmail__inputwrap form__inputwrap form__inputwrap--text">
                                    <label className="form__label txt" htmlFor="firstname">{trad.form.firstname}</label>
                                    <input id="firstname" className="sendmail__input form__input form__input--text" type="text" onFocus={this.animationOnFocus} onBlur={this.animationOnBlur} onKeyUp={this.animationOnKeyUp} />
                                </span>
                            </div>

                            <div className="form__field">
                                <span className="sendmail__inputwrap form__inputwrap form__inputwrap--text">
                                    <label className="form__label txt" htmlFor="mail">{trad.form.mail}</label>
                                    <input id="mail" className="sendmail__input form__input form__input--text" type="text" onFocus={this.animationOnFocus} onBlur={this.animationOnBlur} onKeyUp={this.animationOnKeyUp} />
                                </span>
                            </div>

                            <div className="form__field form__field--check">
                                <span className="form__inputwrap form__inputwrap--check" onClick={this.animationOnCheck}>
                                    <label className="form__label txt">{trad.form.optin_rules}</label>
                                    <input id="rules" className="form__input form__input--checkbox mandatory counted event" type="checkbox" data-label="optin_rules" data-event="TagEvent::Page" value={this.state.valueInputRules} onKeyUp={this.animationOnKeyUp} />
                                </span>
                            </div>

                            <div className="form__field form__field--check">
                                <span className={classNames("form__inputwrap form__inputwrap--check", {"checked": this.state.inputWrapChecked})} onClick={this.animationOnCheck}>
                                    <label className="form__label txt">{trad.form.optin_offers}</label>
                                    <input id="offers" className="form__input form__input--checkbox mandatory counted event" type="checkbox" data-label="optin_offers" data-event="TagEvent::Page" value={this.state.valueInputOffers} onKeyUp={this.animationOnKeyUp} />
                                </span>
                            </div>

                            <div className="form__field form__field--submit">
                                <span className="form__inputwrap form__inputwrap--submit form__inputwrap--disabled">
                                    <span className="form__input form__input--submit btn btn--simple event" data-label="jwalker2_submit" data-event="TagEvent::Page" onClick={this.submit}>{trad.form.submit}</span>
                                </span>
                            </div>
                        </div>

                        <div className={classNames("form__result", {"on": this.state.formResultON})}>
                            <p className="form__result-txt txt">{trad.form.result}</p>
                        </div>

                    </fieldset>
                </form>
            </div>
        );
    }
}

export default Form;