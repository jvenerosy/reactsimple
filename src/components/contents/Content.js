import React, { Component } from 'react';

import YoutubeVideo from '../youtube/YoutubeVideo';
import Slider from '../sliders/Slider';
import Products from '../products/Products';
import Links from '../links/Links';
import Form from '../forms/Form';

class Content extends Component {

    render() {
        return (
            <div>
                <YoutubeVideo videoID="hhY_90WIhec" />
                <Slider />
                <Products />
                <Links />
                <Form />
            </div>
        );
    }
}

export default Content;