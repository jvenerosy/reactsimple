import React, { Component } from 'react';

import Depart from '../travelContainers/Depart';
import Destinations from '../travelContainers/Destinations';

class ContentTravel extends Component {
    render() {
        return (
            <div>
                <Depart />
                <Destinations />
            </div>
        );
    }
}

export default ContentTravel;