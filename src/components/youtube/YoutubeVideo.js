import React, { Component } from 'react';

class YoutubeVideo extends Component {

    render() {
        return (
            <div className="videoContainer">
                <div className="videoWrapper">
                    <iframe width="560" height="315" src={"https://www.youtube.com/embed/" + this.props.videoID} frameBorder="0" allowFullScreen title="video base simple"></iframe>
                </div>
            </div>
        );
    }
}

export default YoutubeVideo;