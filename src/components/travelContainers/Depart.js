import React, { Component } from 'react';

import Modal from '../modal/Modal';

import trad from '../../data/trad/fr';


class Depart extends Component {

    constructor() {
        super();
        this.state = {
            isOpen: false,
            villeDepart: trad.from.filter.select
        };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    /* Ouverture de la modal */
    openModal() {
        this.setState({ isOpen: true });
    }

    /* Fermeture de la modal */
    closeModal() {
        this.setState({ isOpen: false });
    }

    /* Changement de la ville de départ */
    changeVilleDepart(city) {
        this.setState({ villeDepart: city });
        this.closeModal();

        //On retire la classe "mask" pour chaque element contenant cette classe (ici la classe 'destination')
        var destinations = document.getElementsByClassName("destination");
        for(var i = 0; i < destinations.length; i++) {
            destinations[i].classList.remove("mask");
        }
       
    }

    render() {
        return (
            <div className="from">
                <p className="from__text">{trad.from.key1}</p>
                <div className="from__filter">
                    <button className="from__btn event" data-event="TagEvent::Page" data-label="navigation" data-city data-src="#form-select" onClick={this.openModal}>
                        <span className="original">{this.state.villeDepart}</span><img className="from__plus from__plus--original" src={require("../../img/plus.png")} alt="" />
                        <img className="from__close" src={require("../../img/close.png")} alt="" />
                    </button>
                    
                    <Modal
                        show={this.state.isOpen}
                        onClose={this.closeModal}
                    >
                        <div className="from__select" id="form-select">
                            <p className="from__btn">{trad.from.filter.select}<img className="from__plus" src={require("../../img/plus.png")} alt="" /></p>
                            {trad.from.depart.map((city, i) => { //on boucle
                                return (
                                    <p key={i} className="from__select-city event" data-event={"TagEvent::Exit_" + city} data-label={"From:" + city} onClick={this.changeVilleDepart.bind(this, city)}>{city}</p>
                                );
                            })}
                        </div>
                    </Modal>
                </div>
                <p className="from__text">{trad.from.key2}</p>
            </div>
        );
    }
}

export default Depart;