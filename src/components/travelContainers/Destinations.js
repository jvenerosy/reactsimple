import React, { Component } from 'react';

import DestinationCity from './DestinationCity'

import trad from '../../data/trad/fr';

class Destinations extends Component {

    render() {
        return (
            <div className="destinations">
                {trad.cities.map((city, i) => {
                    return (
                        <div className="destination mask" key={i + 1} id={("dest_" + city.title).replace(' ', '_')}>
                            <div className="masked"></div>
                            <DestinationCity index={i + 1} price={city.price} title={city.title} dateStart={city.dateStart} dateEnd={city.dateEnd} text={city.text} destinations={city.destinations} />
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default Destinations;