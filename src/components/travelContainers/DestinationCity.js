import React, { Component } from 'react';
import Modal from '../modal/Modal';

class DestinationCity extends Component {

    constructor() {
        super();
        this.state = {
            isOpen: false
        }
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({ isOpen: true });
    }

    closeModal() {
        this.setState({ isOpen: false });
    }

    render() {
        return (
            <div>
                <div className="destination__city" onClick={this.openModal}>
                    <a data-city data-src={"#city_" + this.props.index}>
                        <img className="destination__mea city_action event" src={require("../../img/travel/destination_0" + this.props.index + ".png")} data-cityVal={this.props.title.replace(' ', '_')} data-event="TagEvent::Page" data-label="navigation" alt="" />
                    </a>
                    <p className="destination__title">{this.props.title}<span className="destination__price">A partir de {this.props.price}</span></p>
                    <span className="destination__plus" data-city data-src={"#city_" + this.props.index}>
                        <span className="city_action event" data-event="TagEvent::Page" data-label="navigation" data-cityVal={this.props.title.replace(' ', '_')}>+</span>
                    </span>
                </div>
                <Modal show={this.state.isOpen} onClose={this.closeModal}>
                    <div className="city__overlay" id={"city_" + this.props.index}>
                        <div className="city__partner">
                            <p className="city__partner-txt">
                                {this.props.title}
                                <strong>A partir de {this.props.price}</strong>
                            </p>
                        </div>
                        <img className="city__bandeau" src={require("../../img/travel/destination_0" + this.props.index + "-01.png")} alt="" />
                        <div className="city__intro">
                            <div className="city__info">
                                <p className="city__name">{this.props.title}</p>
                                <p className="city__date">Voyage entre le {this.props.dateStart} et le {this.props.dateEnd}</p>
                            </div>
                            <div className="city__price">
                                <div className="city__price--1">
                                    <p className="city__price-number">Aller-simple dès<span>{this.props.price}</span></p>
                                </div>
                                <div className="city__price--2">
                                    <p className="city__price-number">Voyagez en 1<sup>ère</sup><span>+ 14€</span></p>
                                </div>
                            </div>
                        </div>
                        <p className="city__text">{this.props.text}</p>
                        <div className="city__picture">
                            {this.props.destinations.map((city, i) => {
                                return (
                                    <img key={i} className="city__picture-img" src={require("../../img/travel/destination_0" + this.props.index + "_city_0" + (i + 1) + ".png")} alt="" />
                                );
                            }, this)}
                        </div>
                        <div className="city__footer">
                            <p className="city__footer__text">Votre hiver toute en douceur</p>
                            <p className="city__footer__other">Partez découvrir { this.props.name }</p>
                            <p className="city__footer__other2">J'y vais avec</p>
                            <a className="city__footer__btn event" data-event="TagEvent::Exit_oui" data-label="Go:{{ city.name }}_oui" href="" id="lien_oui_{{ city.title |replace({' ': '_'}) }}" target="_blank">
                                <img src="" alt="" />
                           </a>
                           <a className="city__footer__btn event" data-event="TagEvent::Exit_trainline" data-label="Go:{{ city.name }}_trainline" href="" id="lien_trainline_{{ city.title |replace({' ': '_'}) }}" target="_blank">
                                <img src="" alt="" />
                           </a>
                        </div>
                    </div>
                </Modal>
            </div>
        )
    }
}

export default DestinationCity;

