import React, { Component } from 'react';

import trad from '../../data/trad/fr';

class FooterCarterose extends Component {

    render() {
        return (
            <div className="footer">
                <div className="footer__wrapper">
                    <div className="footer__logo carterose">
                        <span className="icon icon-carte-rose-footer-1"></span>
                        <span className="icon icon-carte-rose-footer-2"></span>
                        <span className="icon icon-carte-rose-footer-3"></span>
                    </div>
                    <span className="footer__logo icon icon-logo-brand-1">
                        <span className="icon icon-logo-brand-2"></span>
                    </span>
                </div>
                <p className="footer__mentions">&copy;  {(new Date()).getFullYear()} { trad.project }. { trad.footer_rights }</p>
            </div>
        );
    }
}

export default FooterCarterose;