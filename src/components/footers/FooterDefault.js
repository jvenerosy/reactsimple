import React, { Component } from 'react';

import trad from '../../data/trad/fr';

class FooterDefault extends Component {

    render() {
        return (
            <div className="footer">
                <div className="footer__logo-contener">
                    <div className="footer__partnership">
                        <span className="footer__item">
                            <i className="icon icon-logo-vp"></i>
                        </span>
                        <span className="footer__presents">présente</span>
                        <span className="footer__item">
                            <i className="icon icon-brand icon-logo-brand-b"></i>
                        </span>
                        <p className="mention">&copy;  {(new Date()).getFullYear()} { trad.project } | { trad.footer_rights }</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default FooterDefault;