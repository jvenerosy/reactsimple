import React, { Component } from 'react';

import trad from '../../data/trad/fr';

class Links extends Component {

    render() {

        return (
            <div className="links">
                <h3 className="links__title">LIEN UNIVERS</h3>
                <a href="" className="cta dressing-homme event universe-button" data-universe="4756497" data-label="GarageIndigo-CTA" data-event="TagEvent::Marketing"> </a>
                <h3 className="links__title">LIEN PRODUITS</h3>
                {trad.links.map((link,i) => {
                    return (
                        <a key={i} href="" className="jupe event introlink product-button" data-label={link.label} data-event="TagEvent::Marketing" data-universe={link.universID} data-family={link.productID}>{link.label}</a>
                    );
                })}
            </div>
        );
    }
}

export default Links;