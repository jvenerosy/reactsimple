import React, { Component } from 'react';

import trad from '../../data/trad/fr';

class Products extends Component {

    render() {
        return (
            <div className="screen screen--2 product container">
                {trad.blocks.map(function (block, i) { //on boucle
                    return (
                        <div key={i} className={"product__item product__item--" + (i + 1)}>
                            <h1 className="product__title">{block.title}</h1>
                            <a href=""><img className="load-media" src={require("../../img/product_" + (i + 1) + ".jpg")} alt="" /></a>
                            <div className="product__txt">
                                {block.parags.map(function (parag, i) { //on boucle
                                    return (
                                        <p key={i}>{parag}</p>
                                    );
                                }, this)}
                            </div>
                            <a className="btn event universe-button swap product__btn" data-swap="Accéder à la vente" data-label="Button::cta::Je m'inscris à la vente" data-event="TagEvent::Marketing" target="_blank" data-universe="4143322" href="https://secure.fr.vente-privee.com/ns/fr-fr/operation/65966/classic/4143322/catalog" rel="noopener noreferrer">{trad.shop.before}</a>
                        </div>
                    );
                }, this)}
            </div>
        );
    }
}

export default Products;