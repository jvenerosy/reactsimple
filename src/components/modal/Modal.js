import React, { Component } from 'react';

import PropTypes from 'prop-types';

class Modal extends Component {

    constructor(props) {
        super(props);

        this.state = {
            show: this.props.show
        };
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
    }

    componentWillReceiveProps(newProps) {
        //on met à jour le state avec les nouvelles props
        this.setState({ show: newProps.show });
    }

    /* Lors du click à l'extérieure du contenu de la modale (arrière plan en gris opaque) */
    handleOutsideClick() {
        //on ferme la modal
        this.props.onClose();
    }

    render() {

        var styleBackground = {};

        if (this.state.show) {
            styleBackground = {
                opacity: 1,
                zIndex: 200
            }
        }
        else {
            styleBackground = {
                opacity: 0,
                zIndex: -100
            }
        }

        return (
            <div className="backgroundModal" style={styleBackground} onClick={this.handleOutsideClick}>
                <div className="modal" onClick={(e) => {e.stopPropagation()}}>
                    {this.props.children}
                    <div>
                        <button onClick={this.props.onClose}>Close</button>
                    </div>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    onClose: PropTypes.func.isRequired, //pour dire que seul une fonction est requise pour cette props
    show: PropTypes.bool, //pour dire que seul un booléen est requis pour cette props
    children: PropTypes.node //pour dire qu'un élément enfant est requis dans ce composant
};

export default Modal;
