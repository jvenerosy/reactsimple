import React, { Component } from 'react';

import HeaderDev from '../headers/HeaderDev';
import Header from '../headers/Header';
import HeaderMenu from '../headers/menu/HeaderMenu';
import LoaderDefault from '../loaders/LoaderDefault';
import LoaderCarterose from '../loaders/LoaderCarterose';
import Content from '../contents/Content';
import ContentTravel from '../contents/ContentTravel';
import FooterDefault from '../footers/FooterDefault';
import FooterCarterose from '../footers/FooterCarterose';

import Marketing from '../marketings/Marketing';

import { tooltips } from '../../functions/tooltips';

import config from '../../data/config/fr';
import ScrollInfo from '../loaders/ScrollInfo';

class Home extends Component {

    componentDidMount() {
        tooltips();
    }


    render() {
        return (
            <div>
                <ScrollInfo />
                {config.environment === 'development' ?
                    <HeaderDev />
                    :
                    null
                }
                {config.menu === "true" ?
                    <HeaderMenu />
                    :
                    <Header />
                }
                {config.theme !== 'default' ?
                    <LoaderCarterose />
                    :
                    <LoaderDefault />
                }
                {config.structure !== 'default' ?
                    <ContentTravel />
                    :
                    <div>
                        <Content />
                        <Marketing />
                    </div>
                }
                {config.theme !== 'default' ?
                    <FooterCarterose />
                    :
                    <FooterDefault />
                }
            </div>
        );
    }
}

export default Home;