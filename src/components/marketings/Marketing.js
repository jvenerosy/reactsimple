import React, { Component } from 'react';

import parser from 'html-react-parser'; //Afin de parser le contenu JSON contenant des balises HTML en 'vrai' balises HTML
import classNames from 'classnames';

import trad from '../../data/trad/fr';

require('waypoints/lib/noframework.waypoints.js');

class Marketing extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            height: 0,
            classFixed: false
        };
        this.marketing = this.marketing.bind(this);
    }

    componentDidMount() {
        window.addEventListener('resize', this.marketing);
        this.marketing();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.marketing);
    };

    marketing() {
        var wrapper = document.getElementById('waypoint_wrapper');
        var footer = document.getElementsByClassName("footer");

        if (footer !== null) {
            new window.Waypoint({
                element: wrapper,
                handler: (dir) => {
                    dir === 'down' ? this.setState({classFixed: false}) : this.setState({classFixed: true});
                },
                offset: 'bottom-in-view'
            })
        }

        new window.Waypoint({
            element: document.body,
            handler: (dir) => {
                dir === 'down' ? this.setState({classFixed: true}) : this.setState({classFixed: false});
            },
            offset: -10
        })
    }

    render() {
        return (
            <div className="fixable">
                <div id="waypoint_wrapper" className="marketing js-waypoint-wrapper">
                    <div id="waypoint_content" className={classNames("marketing__wrapper js-waypoint-content", { "fixed": this.state.classFixed })}>
                        <div className="marketing__inner">
                            <div className="marketing__txt">
                                <p className="marketing__txt--1">{trad.marketing.txt1}</p>
                                <p className="marketing__txt--2">{parser(trad.marketing.txt2)}</p>
                            </div>
                            <p className="marketing__txt--3">
                                {parser(trad.marketing.txt3)}
                            </p>
                            <a target="_blank" href={trad.link.generic} className="marketing__cta btn event" data-event="TagEvent::Exit" data-label="Trouvez un salon">{trad.marketing.cta}</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Marketing;