import React, { Component } from 'react';

import $ from "jquery";

class ScrollInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            percent: 0
        }
    }

    componentDidMount() {
        this.display();
        $(window).scroll(this.display).resize(this.display);
    }

    display = () => {
        let docH = $(document).height();
        let winH = $(window).height();
        let scrollTop = $(window).scrollTop();

        let percent = (scrollTop / (docH - winH)) * 100;

        this.setState({ percent: percent + '%' });
    }

    render() {
        return (
            <div className="scrollInfo">
                <div class="scrollInfo__percent" style={{ width: this.state.percent }}></div>
            </div>
        )
    }
}

export default ScrollInfo;