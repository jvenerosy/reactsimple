import React, { Component } from 'react';

import { loader } from '../../functions/loader';

class LoaderCarterose extends Component {

    componentDidMount(){
        loader();
    }

    render() {
        return (
            <div className="loader">
                <div className="loader__content">
                    <div className="loader__logo carterose">
                        <span className="icon icon-carte-rose-1"></span>
                        <span className="icon icon-carte-rose-2"></span>
                        <span className="icon icon-carte-rose-3"></span>
                    </div>

                    <div className="loader__brand">
                        <span className="icon icon-logo-brand"></span>
                    </div>
                    <div className="loader__spin">
                        <svg className="loader__circular" viewBox="25 25 50 50">
                            <circle className="loader__path" cx="50" cy="50" r="20" fill="none" strokeWidth="2" strokeMiterlimit="10" />
                        </svg>
                    </div>
                </div>
            </div>
        );
    }
}

export default LoaderCarterose;