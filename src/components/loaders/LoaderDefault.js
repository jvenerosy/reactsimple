import React, { Component } from 'react';

import { loader } from '../../functions/loader';

import trad from '../../data/trad/fr';

class LoaderDefault extends Component {

    componentDidMount() {
        loader();
    }

    render() {
        return (
            <div className="loader">
                <h2 className="loader__title">{ trad.loader.title }</h2>
                <div className="loader__logo">
                    <img src="http://placehold.it/100x100" alt="à remplacer" />
                </div>
                <p className="loader__parag">{ trad.loader.parag }</p>
                <div className="loader__spin">
                    <svg className="loader__circular" viewBox="25 25 50 50">
                        <circle className="loader__path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>
        );
    }
}

export default LoaderDefault;