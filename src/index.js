import 'core-js/es6/map';
import 'core-js/es6/set';
import 'raf/polyfill';
import 'url-search-params-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';
import config from './data/config/fr';

import loadjs from 'loadjs';
import mixpanel from 'mixpanel-browser';
import vhCheck from "vh-check";

import { $_GET } from './functions/utils/$_GET';
import { getCookieID } from './functions/utils/getCookieID';
import { getUrlParameter } from './functions/utils/getUrlParameter';
import { getMixpanelConfig } from './functions/utils/getMixpanelConfig';
import { tracking } from './functions/utils/tracking';
import { link } from './functions/utils/link';
import { scrollTo } from './functions/scrollTo';

import Home from './components/pages/Home';

var pathCSS = `./style/css/theme-${(config && config.theme) ? config.theme : 'default'}.css`;
require("" + pathCSS);

if (config.structure === "travel") {
    require('./style/css/travel.css');
}
//Récupération de l'élément <html />
var html = document.getElementsByTagName("HTML")[0];

//Changement dynamique du titre dans l'onglet
document.title = config.operation.titre;

//Ajout de la librairie Modernizr si boolean à true
if (config.modules.modernizr) {
    var modernizr_tag = document.createElement('script');
    modernizr_tag.src = "https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js";
    html.appendChild(modernizr_tag);
}

//Ajout de la librairie Detectizr si boolean à true
if (config.modules.detectizr) {
    var detectizr_tag = document.createElement('script');
    detectizr_tag.src = "https://cdnjs.cloudflare.com/ajax/libs/detectizr/2.2.0/detectizr.min.js";
    html.appendChild(detectizr_tag);
}

//Ajout de la librairie Fancybox si boolean à true
if (config.modules.fancybox) {
    var fancybox_tag = document.createElement('script');
    fancybox_tag.src = "https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.20/jquery.fancybox.min.js";
    html.appendChild(fancybox_tag);
}

//Ajout du fichier CSS animate.css su boolean à true 
if (config.modules.animate) {
    var animate_tag = document.createElement('link');
    animate_tag.rel = "stylesheet";
    animate_tag.href = "https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css"
    html.appendChild(animate_tag);
}

if (config.modules.vhFixed) {
    vhCheck({
        bind: false,
        redefineVh: true,
    });
}

var mixpanelConfig = getMixpanelConfig();

var token = mixpanelConfig.token;
mixpanel.init(token, { api_host: mixpanelConfig.apiHost, disable_notifications: true, autotrack: false, track_pageview: false });

//Ajout de Adotmob si boolean à true
if (config.modules.adotmob) {

    var infoMember = getCookieID('infoMember');

    // define a dependency bundle and execute code when it loads
    loadjs(['https://storage.data-vp.com/vp/t.js', 'https://cdn.data-vp.com/common/vpas.js'], 'foobar');

    loadjs.ready('foobar', function () {

        if (infoMember != null) {
            infoMember = infoMember.substr(4); // VALEUR FINAL
            window.adotmob.init(infoMember, config.operation.code, config.operation.code + '_tracking');
        }
        else {
            window.adotmob.init(config.operation.code + '_member', config.operation.code, config.operation.code + '_tracking');
        }
    });
}

/*var currentCountry = 'fr'; //pour l'instant on est que sur des sites français
        var ua = navigator.userAgent.toLowerCase();*/

//RECUPERATION DES PROPRIETES MIXPANEL
var mp_properties = getUrlParameter('mp_properties', null);
var propriet = [];

if (mp_properties) {
    mp_properties = decodeURIComponent(mp_properties);
    mp_properties = atob(mp_properties);
    propriet = JSON.parse(mp_properties);
}

var browserType;
var u = navigator.userAgent.toLowerCase();

if (u.indexOf("vp-iphone") > -1 || u.indexOf("vp-ipad") > -1 || u.indexOf("vp-android-smartphone") > -1) {

    // Application VP
    browserType = "Application Vente-privee";

} else if ($_GET('FromApp') && ($_GET('FromApp') === true || $_GET('FromApp') === 'true')) {

    browserType = "FromApp";

} else if ($_GET('FromWebApp') && ($_GET('FromWebApp') === true || $_GET('FromWebApp') === 'true')) {

    browserType = "FromWebApp";

} else if (html.classList.contains("mobile")) {

    // Mobile
    browserType = "Mobile";

} else {

    // Probably Desktop
    browserType = "Desktop";

}

mixpanel.identify(propriet['Member ID']);

mixpanel.register({
    "Interface": propriet['Interface'],
    "UTM Term [Last Touch]": propriet['UTM Term [Last Touch]'],
    "UTM Content [Last Touch]": propriet['UTM Content [Last Touch]'],
    "UTM Campaign [Last Touch]": propriet['UTM Campaign [Last Touch]'],
    "UTM Medium [Last Touch]": propriet['UTM Medium [Last Touch]'],
    "UTM Source [Last Touch]": propriet['UTM Source [Last Touch]'],
    "CRM Segment": propriet['CRM Segment'],
    "# of Completed Purchases": propriet['# of Completed Purchases'],
    "Member ID": propriet['Member ID'],
    // eslint-disable-next-line
    "Optimizely Current A\/B Tests": propriet['Optimizely Current A\/B Tests'],
    "Source": propriet['Source'],
    "Point Reached On Homepage": propriet['Point Reached On Homepage']
});

mixpanel.track("View B2B mini website", {
    "Operation Code": config.operation.code,
    "Device width": window.innerWidth,
    "Device height": window.innerHeight,
    "Browser type": browserType
});

//Lancement du tracking sur les éléments HTML
tracking();

ReactDOM.render(<Home />, document.getElementById('root'), () => {
    //Lancement de la création des liens pour les sites Marketing
    link();

    //Lancement de la fonction scrollTo
    scrollTo('.scrollTo');
});
registerServiceWorker();