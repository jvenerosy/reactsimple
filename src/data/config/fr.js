module.exports = {
    "url": {
        "normal": "http://secure.fr.vente-privee.com/vp4/_sales/MINISITES1/opeSpe/",
        "secure": "https://secure.fr.vente-privee.com/vp4/_sales/MINISITES1/opeSpe/",
        "desktop": "https://secure.fr.vente-privee.com/homev6/fr/Operation/Access/",
        "device": "https://m.vente-privee.com/w2/index.html#operation/",
        "application": "appvp://operation/",
        "oldApp": "applivp://OperationAccess/operationid/",
        "productLinkWeb": "https://secure.fr.vente-privee.com/ns/fr-fr/operation/",
        "productLinkMobile": "https://m.vente-privee.com/w2/#operation/",
        "productLinkApp": "appvp://fp/",
        "SpecialEventLinkApp": "appvp://specialcatalog/",
        "SpecialEventWebApp": "https://m.vente-privee.com/w2/index.html#specialcatalog/",
        "token": "a3cb8bb25549b48e7409f1211fc04976"
    },
    "environment": "development", // development | prod
    "modules": {
        "jquery": true,
        "detectizr": true,
        "modernizr": true,
        "waypoints": true,
        "slick": true,
        "adotmob": true,
        "vhFixed": true,
        "animate": true,
        "fancybox": true
    },
    "mixpanel": {
        "prod": {
            "token": "a3cb8bb25549b48e7409f1211fc04976",
            "apiHost": "https://data.services.vente-privee.com/frontservices/api-tracking",
            "apiHashMemberId": "https://brand-mail-api.front.vpgrp.net/opespe/"
        },
        "preprod": {
            "token": "eae7ed6b6420b0befde82c2e721ae890",
            "apiHost": "https://data.services.preprod.vente-privee.com/frontservices/api-tracking",
            "apiHashMemberId": "https://brand-mail-api-preprod.front.vpgrp.io/opespe/"
        }
    },
    "operation": {
        "code": "ReactSimple",
        "id": "63387",
        "partnerID": "18739",
        "date": "27/09/2026 22:00",
        "titre": "METTRE UN TITRE",
        "description": "AINSI QU'UNE DESCRIPTION"
    },
    "loader": {
        "jQuerySelector": ".load-media",
    },
    "marketingButton": {
        "beforeSale": "S’inscrire à la vente",
        "duringSale": "Accéder à la vente"
    },
    "xtsite": {
        "appIphone": "538740",
        "appIpad": "538748",
        "appAandroidSmartphone": "538758",
        "appAndroidTablet": "563561",
        "smartphone": "451466",
        "default": "337001"
    },
    "theme": "carterose", // default | carterose
    "structure": "default", // default | travel
    "menu": "false" // true | false
}
