module.exports = {
    "project": "[NOM DU PROJET]",
    "generic_button": "Je l'offre",
    "generic_url": "http://www.google.fr",
    "footer_title": "<strong className='highlight'>Commandez la carte cadeau Air France </strong>avant le 21/12 à offrir à vos proches pour la destination de leur choix.",
    "footer_rights": "Tous droits réservés.",
    "loader": {
        "title": "Invité de marque",
        "parag": "C'est un super paragraphe"
    },
    "menus": [
        {
            "title": "Menu 1",
            "slug": "menu1"
        },
        {
            "title": "Menu 2",
            "slug": "menu2"
        },
        {
            "title": "Menu 3",
            "slug": "menu3",
            "additionalClass": "mob-only"
        }
    ],
    "menu_external": "Lien externe",
    "shop": {
        "before": "Je m'inscris",
        "after": "J'achète"
    },
    "links": [
        {
            "universID": "4756497",
            "productID": "11764700",
            "label": "Jupe argentée"
        },
        {
            "universID": "4756497",
            "productID": "11764672",
            "label": "Combinaison"
        }
    ],
    "blocks": [
        {
            "product": {
                "title": "Academy",
                "id": "9636766"
            },
            "title": "GREGORY",
            "parags": [
                "Dolore in aute ad occaecat id. Voluptate eiusmod sit culpa duis qui laboris irure veniam mollit adipisicing Lorem. Adipisicing ullamco qui cupidatat ea consequat consequat tempor ex ea occaecat fugiat qui. Laborum tempor consequat cupidatat adipisicing velit est cupidatat sit anim ut enim amet. Reprehenderit mollit elit ea cupidatat dolore.",
                "Lorem laboris non officia laboris et est ex nisi officia duis occaecat. Minim amet ut id culpa. Consequat irure Lorem quis ut deserunt. Duis ullamco sint incididunt deserunt enim exercitation Lorem ex sit voluptate anim reprehenderit enim aliqua. Minim aute deserunt ad incididunt cillum dolore quis."
            ]
        },
        {
            "product": {
                "title": "Roadster",
                "id": "9636733"
            },
            "title": "JORDY",
            "parags": [
                "Ipsum laborum aliquip pariatur dolore do fugiat amet minim irure ipsum irure ad. Quis commodo officia do voluptate est occaecat adipisicing. Non reprehenderit ex irure commodo voluptate enim dolor sunt occaecat ea. Qui Lorem et mollit sunt ut magna id nostrud consectetur ea culpa. Veniam adipisicing nisi duis deserunt labore aliqua reprehenderit tempor veniam labore.",
                "Ipsum enim reprehenderit labore ad sit aute in ex esse excepteur do non. Eu pariatur anim nulla laborum sint. Elit magna officia sunt ut velit irure mollit. Ea sit aute et nisi. Quis aliqua eu aute id non nulla."
            ]
        },
        {
            "product": {
                "title": "Ultrasize",
                "id": "9636715"
            },
            "title": "SEB",
            "parags": [
                "Aliqua magna sint commodo laborum id qui reprehenderit eiusmod tempor. Do sunt ex aliquip nisi reprehenderit cillum irure esse velit. Est ullamco esse aute consequat incididunt incididunt. In elit in occaecat deserunt eiusmod dolore velit incididunt. Quis amet sit veniam laborum.",
                "Quis eu ut deserunt eu reprehenderit. Esse proident voluptate pariatur sunt excepteur elit in eiusmod laboris esse. Ut qui nostrud sint do minim proident veniam occaecat. Aliquip ex aliqua duis voluptate pariatur exercitation labore tempor fugiat sunt Lorem aute sit. Ullamco velit ut non id proident.",
            ]
        }
    ],
    "marketing": {
        "txt1": "Offre d’Exception",
        "txt2": "10&nbsp;€ remboursés",
        "txt3": "<b>Mentions encart marketing</b> A compléter",
        "cta": "CALL TO ACTION",
    },
    "link": {
        "generic": "https://www.kerastase.fr/les-jours-exception?utm_source=vente-privee&utm_medium=website_partnership&utm_content=ker_haca__other_cv&utm_campaign=vente-privee-jours-exception_&utm_term=partnership-vp-kerastase",
    },
    "from": {
        "key1": "D'où partez vous ?",
        "key2": "Où vous emmène-t-on ?",
        "filter": {
            "select": "Sélectionnez votre ville de départ"
        },
        "depart": [
            "AIX EN PROVENCE TGV",
            "AVIGNON TGV",
            "LYON",
            "MARSEILLE",
            "MONTPELLIER",
            "NIMES",
            "PARIS GARE DE LYON",
            "VALENCE TGV"
        ]
    },
    "form": {
        "title": "Inscrivez-vous au tirage&nbsp;au&nbsp;sort&nbsp;!",
        "lastname": "Nom",
        "firstname": "Prénom",
        "mail": "Adresse mail",
        "optin_rules": "J'accepte le règlement",
        "optin_offers": "J'accepte de recevoir les nouveautés",
        "submit": "Je participe",
        "result": "Merci pour votre <br />participation !",
    },
    "cities": [
        {
            "title": "BARCELONA SANTS",
            "name": "Barcelone",
            "price": "25€ ",
            "dateStart": "09/01",
            "dateEnd": "21/03",
            "text": "La ville créative qui vit face à la mer ! La capitale catalane regorge de trésors architecturaux et possède un riche patrimoine. Sa diversité culturelle fait d'elle un lieu unique. Dynamique et cosmopolite, Barcelone est une ville qui séduit tant par les édifices modernistes d'Antoni Gaudí, que par son irrésistible et ancien quartier Gothique ou son mode de vie vibrant et animé.",
            "destinations": [
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESBST&eurl=https://www.oui.sncf/calendar/FRAIE/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avignon-tgv&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESBST&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=BARCELONA SANTS (BARCELONE)&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=lyon&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRLPD_ESBST&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=BARCELONA SANTS (BARCELONE)&vscTrainOrigin=LYON&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMSC_FRXNA&eurl=https://www.oui.sncf/calendar/FRMRS/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESBST&eurl=https://www.oui.sncf/calendar/FRMPL/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESBST&eurl=https://www.oui.sncf/calendar/FRFNI/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "PARIS GARE DE LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=paris&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=ESBST_FRPLY&eurl=https://www.oui.sncf/calendar/FRPAR/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr"
                        },
                        {
                            "name": "VALENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=valence-tgv&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRVLA_ESBST&eurl=https://www.oui.sncf/calendar/FRVAV/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                },
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESBST&eurl=https://www.oui.sncf/calendar/FRAIE/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avignon-tgv&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESBST&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=BARCELONA SANTS (BARCELONE)&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=lyon&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRLPD_ESBST&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=BARCELONA SANTS (BARCELONE)&vscTrainOrigin=LYON&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMSC_FRXNA&eurl=https://www.oui.sncf/calendar/FRMRS/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESBST&eurl=https://www.oui.sncf/calendar/FRMPL/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESBST&eurl=https://www.oui.sncf/calendar/FRFNI/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "PARIS GARE DE LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=lyon&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRPLY_ESBST&eurl=https://www.oui.sncf/calendar/FRPAR/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "VALENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=valence-tgv&destination=barcelona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRVLA_ESBST&eurl=https://www.oui.sncf/calendar/FRVAV/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                }
            ]
        },
        {
            "title": "FIGUERAS",
            "name": "Figueres",
            "price": "25€ ",
            "dateStart": "09/01",
            "dateEnd": "21/03",
            "text": "Située à proximité de la frontière Franco-espagnole, cette ville aux profondes racines catalanes est un point d’arrêt incontournable. Berceau de Salvador Dali, Figueras invite les visiteurs à la découverte du Théâtre-Musée Dali et propose en outre, une offre culturelle complète grâce à une multitude d’événements et de fêtes répartis sur toute l’année. C’est aussi le point de départ idéal pour aller découvrir les plus belles plages de la Costa Brava de Cadaqués à l’Escala, et découvrir les villages de pêcheurs typiques de la région.",
            "destinations": [
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESFIG&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=FIGUERAS&vscTrainOrigin=AIX EN PROVENCE TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avigon&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESBST&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=BARCELONA SANTS (BARCELONE)&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=lyon&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRLPD_ESBST&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=BARCELONA SANTS (BARCELONE)&vscTrainOrigin=LYON&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMSC_ESFIG&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=FIGUERAS&vscTrainOrigin=MARSEILLE&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESFIG&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=FIGUERAS&vscTrainOrigin=MONTPELLIER&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESFIG&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=FIGUERAS&vscTrainOrigin=NIMES&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "PARIS GARE DE LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=paris&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRPLY_ESFIG&eurl=https://www.oui.sncf/calendar/FRPAR/ESFRS/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "VALENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=valence-tgv&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRVLA_ESFIG&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=FIGUERAS&vscTrainOrigin=VALENCE (26)&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                },
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESFIG&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=FIGUERAS&vscTrainOrigin=AIX EN PROVENCE TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avigon&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESBST&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=BARCELONA SANTS (BARCELONE)&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=lyon&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRLPD_ESBST&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=BARCELONA SANTS (BARCELONE)&vscTrainOrigin=LYON&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMSC_ESFIG&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=FIGUERAS&vscTrainOrigin=MARSEILLE&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESFIG&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=FIGUERAS&vscTrainOrigin=MONTPELLIER&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESFIG&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=FIGUERAS&vscTrainOrigin=NIMES&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "PARIS GARE DE LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=paris&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRPLY_ESFIG&eurl=https://www.oui.sncf/calendar/FRPAR/ESFRS/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "VALENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=valence-tgv&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRVLA_ESFIG&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=FIGUERAS&vscTrainOrigin=VALENCE (26)&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                }
            ]
        },
        {
            "title": "GERONE",
            "name": "Gérone",
            "price": "25€ ",
            "dateStart": "09/01",
            "dateEnd": "21/03",
            "text": "Première d’Espagne pour sa qualité de vie, Gérone est une ville à la fois discrète et authentique. Si elle séduit au prime abord par la richesse de son patrimoine architectural, elle propose également une importante offre culturelle. Ses nombreux musées, festivals et restaurants en font une ville vivante toute l'année. Parcourez les rues de sa vieille ville unique et découvrez deux mille ans d'histoire !",
            "destinations": [
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=AIX EN PROVENCE TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avignon-tgv&destination=gerona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=lyon&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRLPD_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=LYON&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESBST&eurl=https://www.oui.sncf/calendar/FRAIE/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=MONTPELLIER&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=NIMES&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "PARIS GARE DE LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=paris&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRPLY_ESGRO&eurl=https://www.oui.sncf/calendar/FRPAR/ESGRO/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "VALENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=valence-tgv&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRVLA_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=VALENCE (26)&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                },
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=figueres-vilafant",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=AIX EN PROVENCE TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avignon-tgv&destination=gerona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=lyon&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRLPD_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=LYON&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESBST&eurl=https://www.oui.sncf/calendar/FRAIE/ESBCN/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=MONTPELLIER&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=NIMES&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "PARIS GARE DE LYON",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=paris&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRPLY_ESGRO&eurl=https://www.oui.sncf/calendar/FRPAR/ESGRO/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "VALENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=valence-tgv&destination=girona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRVLA_ESGRO&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=GERONE (GIRONA)&vscTrainOrigin=VALENCE (26)&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                }
            ]
        },
        {
            "title": "MADRID ATOCHA",
            "name": "Madrid",
            "price": "35€ ",
            "dateStart": "09/01",
            "dateEnd": "21/03",
            "text": "Madrid est gastronomie, art et \"fiesta\" ! Capitale et centre financier du pays, Madrid est une ville qui combine des infrastructures modernes comme les tours de la Puerta de Europa avec un riche patrimoine culturel, artistique et architectural, notamment sa célèbre vieille ville ou le Madrid des Austrias. Promenez-vous dans ses quartiers modernes, faîtes une halte sur le bord de l'étang du Retiro ou prenez un verre dans la vieille ville !",
            "destinations": [
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=madrid",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESMAD&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=MADRID ATOCHA&vscTrainOrigin=AIX EN PROVENCE TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avignon-tgv&destination=madrid",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESMAD&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=MADRID ATOCHA&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=madrid",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMSC_ESMAD&eurl=https://www.oui.sncf/calendar/FRMRS/ESMAD/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=madrid",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESMAD&eurl=https://www.oui.sncf/calendar/FRMPL/ESMAD/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=madrid",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESMAD&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=MADRID ATOCHA&vscTrainOrigin=NIMES&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                },
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=madrid",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESMAD&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=MADRID ATOCHA&vscTrainOrigin=AIX EN PROVENCE TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avignon-tgv&destination=madrid",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESMAD&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=MADRID ATOCHA&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=madrid",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMSC_ESMAD&eurl=https://www.oui.sncf/calendar/FRMRS/ESMAD/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=madrid",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESMAD&eurl=https://www.oui.sncf/calendar/FRMPL/ESMAD/20180112/ONE_WAY/2/26-NO_CARD?onlyDirectTrains=false&lang=fr&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=madrid",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESMAD&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=MADRID ATOCHA&vscTrainOrigin=NIMES&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                }
            ]
        },
        {
            "title": "SARAGOSSE",
            "name": "Saragosse",
            "price": "35€ ",
            "dateStart": "09/01",
            "dateEnd": "21/03",
            "text": "Sur les rives de l'Èbre, la capitale aragonaise également surnommée la ville des quatre cultures, conserve dans ses rues, un riche héritage architectural. En effet, Romains, Musulmans, Juifs et Chrétiens y ont tous laissé une empreinte forte et unique. Une promenade dans les quartiers les plus anciens vous permettra de contempler les principaux joyaux de la ville, notamment la Basilique du Pilar, la Seo, ou le Palais de la Aljaferia parmi tant d’autres.",
            "destinations": [
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=zaragoza-delicias",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESZAZ&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=SARAGOSSE (ZARAGOZA DELICIAS)&vscTrainOrigin=AIX EN PROVENCE TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avignon-tgv&destination=zaragoza-delicias",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESZAZ&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=SARAGOSSE (ZARAGOZA DELICIAS)&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=zaragoza-delicias",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMSC_ESZAZ&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=SARAGOSSE (ZARAGOZA DELICIAS)&vscTrainOrigin=MARSEILLE&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=zaragoza-delicias",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESZAZ&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=SARAGOSSE (ZARAGOZA DELICIAS)&vscTrainOrigin=MONTPELLIER&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=zaragoza-delicias",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESZAZ&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=SARAGOSSE (ZARAGOZA DELICIAS)&vscTrainOrigin=NIMES&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                },
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=zaragoza-delicias",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESZAZ&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=SARAGOSSE (ZARAGOZA DELICIAS)&vscTrainOrigin=AIX EN PROVENCE TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avignon-tgv&destination=zaragoza-delicias",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESZAZ&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=SARAGOSSE (ZARAGOZA DELICIAS)&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=zaragoza-delicias",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMSC_ESZAZ&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=SARAGOSSE (ZARAGOZA DELICIAS)&vscTrainOrigin=MARSEILLE&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=zaragoza-delicias",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESZAZ&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=SARAGOSSE (ZARAGOZA DELICIAS)&vscTrainOrigin=MONTPELLIER&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=zaragoza-delicias",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESZAZ&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=SARAGOSSE (ZARAGOZA DELICIAS)&vscTrainOrigin=NIMES&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                }
            ]
        },
        {
            "title": "TARRAGONE",
            "name": "Tarragone",
            "price": "35€ ",
            "dateStart": "09/01",
            "dateEnd": "21/03",
            "text": "Au bord de la Méditerranée, cette ville séduit par son riche héritage romain et son ensemble archéologique déclaré Patrimoine mondial par l'UNESCO. La promenade dans la vieille ville entourée de murailles évoque sans conteste son histoire lointaine. Outre cet important héritage monumental, Tarragone séduit par son patrimoine naturel et ses longues plages.",
            "destinations": [
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=tarragona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESQGN&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=TARRAGONA (ES)&vscTrainOrigin=AIX EN PROVENCE TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avignon-tgv&destination=tarragona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESQGN&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=TARRAGONA (ES)&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=tarragona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMSC_ESQGN&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=TARRAGONA (ES)&vscTrainOrigin=MARSEILLE&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=tarragona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESQGN&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=TARRAGONA (ES)&vscTrainOrigin=MONTPELLIER&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=tarragona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESQGN&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=TARRAGONA (ES)&vscTrainOrigin=NIMES&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                },
                {
                    "from": [
                        {
                            "name": "AIX EN PROVENCE TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=aix-en-provence-tgv&destination=tarragona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAIE_ESQGN&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=TARRAGONA (ES)&vscTrainOrigin=AIX EN PROVENCE TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "AVIGNON TGV",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=avignon-tgv&destination=tarragona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRAVG_ESQGN&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=TARRAGONA (ES)&vscTrainOrigin=AVIGNON TGV&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MARSEILLE",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=marseille&destination=tarragona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMSC_ESQGN&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=TARRAGONA (ES)&vscTrainOrigin=MARSEILLE&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "MONTPELLIER",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=montpellier&destination=tarragona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRMPL_ESQGN&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=TARRAGONA (ES)&vscTrainOrigin=MONTPELLIER&PREX=T_PAR_5307708CA87AD"
                        },
                        {
                            "name": "NIMES",
                            "trainline": "https://www.trainline.eu/affiliate/v1?source=340176&origin=nimes&destination=tarragona",
                            "oui": "https://v.oui.sncf/dynclick/vsc-fr/?ept-publisher=Vente_privee&ept-name=Part_Ventes_privees_decembre_Elipsos&eseg-name=OD&eseg-item=FRFNI_ESQGN&eurl=https://www.oui.sncf/promotion-train/multi/promo-tgv-Espagne?vscTrainDestination=TARRAGONA (ES)&vscTrainOrigin=NIMES&PREX=T_PAR_5307708CA87AD"
                        }
                    ]
                }
            ]
        },
    ]
};