#!/usr/bin/env bash

# Project configuration
jsonFile='./src/data/config/fr.js';

echo -e "0. pré-prod"
echo -e "1. prod"

re='^[0-9]+$'

while [ -z $number ] || ! [[ $number =~ $re ]]; do
  read -p  'Choisissez le numéro pour lequel vous voulez compiler (0=pré-prod, 1=prod) : ' -n 1 number
done

if [ $number = 0 ]; then
    echo -e "\n\nVous avez choisi une compilation pour la pré-prod !"
else
    echo -e "\n\nVous avez choisi une compilation pour la prod !"
fi   


# Update project configuration file according to custom configs
node > ./package.tmp.json <<EOF
// Read data
var data = require('./${jsonFile}');
var package = require('./package.json');

var url = data.url.secure + data.operation.code;

if(${number} == 0) {
    url = "http://df.vente-privee.com/vp4/_sales/MINISITES1/opeSpe/" + data.operation.code
}

// Manipulate data
package.homepage = url;

// Output data
console.log(JSON.stringify(package, null, "\t"));
EOF

mv ./package.tmp.json ./package.json